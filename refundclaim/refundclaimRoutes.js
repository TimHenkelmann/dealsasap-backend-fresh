/**
 * Created by yunusemre on 30.06.2016.
 */

module.exports = refundclaimRoutes;

function refundclaimRoutes(passport) {

    var refundclaimController = require('./refundclaimController');
    var router = require('express').Router();
    var unless = require('express-unless');
    var multiparty = require('connect-multiparty');
    var multipartyMiddleware = multiparty();

    var mw = passport.authenticate('jwt', {session: false});
    mw.unless = unless;

    //middleware
    router.use(mw.unless({method: ['GET', 'OPTIONS']}));

    router.route('/refundclaim')
        .post(multipartyMiddleware,refundclaimController.postRefundclaim)
        .get(refundclaimController.getRefundclaims);

    router.route('/refundclaim/:refundclaim_id')
        .get(refundclaimController.getRefundclaim)
        .put(refundclaimController.putRefundclaim);
    

    router.route('/refundclaim/user/:user_id')
        .get(refundclaimController.getRefundclaimByUser);

    return router;
}



