/**
 * Created by yunusemre on 30.06.2016.
 */


var mongoose = require('mongoose');


// Define our movie schema
var RefundclaimSchema   = new mongoose.Schema({
    transaction: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Offerresponse'
    },
    buyer:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    seller:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    pictures: [{
        data: String, contentType: String, name: String
    }],
    message: String,
    date: Date,
    isResolved: {type: Boolean, default: false}

});

// Export the Mongoose model
module.exports = mongoose.model('Refundclaim', RefundclaimSchema);