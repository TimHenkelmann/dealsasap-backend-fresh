/**
 * Created by yunusemre on 30.06.2016.
 */

var Refundclaim = require('./refundclaimSchema');


exports.postRefundclaim = function (req, res) {
    console.log("hi");
    console.log(req.files);
    var refundclaim = new Refundclaim(req.body);
    var fs = require('fs');
    var fileArray = req.files.file;

    function base64_encode(file) {
        // read binary data
        var bitmap = fs.readFileSync(file);
        // convert binary data to base64 encoded string
        return new Buffer(bitmap).toString('base64');
    }
    refundclaim.pictures = fileArray;
    if (fileArray) {
        console.log('found files');
        for (var i = 0, len = fileArray.length; i < len; i++) {
            refundclaim.pictures[i].data = base64_encode(req.files.file[i].path);
            refundclaim.pictures[i].contentType = req.files.file[i].type;
            refundclaim.pictures[0].name = req.files.file[0].name;
        }
        ;


    }
    ;
    console.log(fileArray);
    refundclaim.save(function (err, m) {
        if (err) {
            res.status(500).send(err);
            return;
        }

        res.status(201).json(m);
    });
};

exports.getRefundclaims = function (req, res) {
    Refundclaim.find(function (err, refundclaims) {
        if (err) {
            res.status(500).send(err);
            return;
        }
        res.json(refundclaims);
    });
};


// Create endpoint /api/buyingoffers/:buyingoffer_id for GET
exports.getRefundclaim = function (req, res) {
    // Use the Beer model to find a specific beer
    Refundclaim.findById(req.params.refundclaim_id, function (err, refundclaim) {

        if (err) {
            res.status(500).send(err)
            return;
        }
        ;

        res.json(refundclaim);
    });
};


exports.getRefundclaimByUser = function (req, res) {
    Refundclaim.find({'sender': req.params.user_id}, function (err, refundclaims) {
        if (err) {
            res.status(500).send(err)
            return;
        }

        res.json(refundclaims);
    });
};

exports.putRefundclaim = function(req, res) {
    Refundclaim.findByIdAndUpdate(
        req.params.refundclaim_id,
        req.body,
        {
            //pass the new object to cb function
            new: true,
            //run validations
            runValidators: true
        }, function (err, refundclaim) {
            if (err) {
                res.status(500).send(err);
                return;
            }
            res.json(refundclaim);
        });

};
