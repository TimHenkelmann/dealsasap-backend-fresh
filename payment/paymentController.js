/**
 * Created by yunusemre on 2.07.2016.
 */

var Payment = require('./paymentSchema');



exports.postPayment = function(req, res) {

    var payment = new Payment(req.body);

    //do not allow user to fake identity. The user who posted the buying offer must be the same user that is logged in
    payment.save(function(err, m) {
        if (err) {
            res.status(500).send(err);
            return;
        }

        res.status(201).json(m);
    });
};



exports.putPayment = function(req, res) {
    Payment.findByIdAndUpdate(
        req.params.payment_id,
        req.body,
        {
            //pass the new object to cb function
            new: true,
            //run validations
            runValidators: true
        }, function (err, payment) {
            if (err) {
                res.status(500).send(err);
                return;
            }
            res.json(payment);
        });

};


exports.getPayment = function(req, res) {
    Payment.find({
        user: req.params.user_id
    }, function(err, payment) {
        if (err) {
            return res.status(400).send(err);
        }
        else {
            res.json(payment);
        }
    });
};
