/**
 * Created by yunusemre on 2.07.2016.
 */

// Load required packages
var mongoose = require('mongoose');

// Define our movie schema
var PaymentSchema   = new mongoose.Schema({
    name: String,
    cardnumber: Number,
    expiremonth: Number,
    expireyear: Number,
    cvvcode: Number,
    cardtype: String,
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'}
});

// Export the Mongoose model
module.exports = mongoose.model('Payment', PaymentSchema);