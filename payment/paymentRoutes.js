/**
 * Created by yunusemre on 2.07.2016.
 */

module.exports = paymentRoutes;

function paymentRoutes(passport) {

    var paymentController = require('./paymentController');
    var router = require('express').Router();
    var unless = require('express-unless');

    var mw = passport.authenticate('jwt', {session: false});
    mw.unless = unless;

    //middleware
    router.use(mw.unless({method: ['GET', 'OPTIONS']}));

    router.route('/payment')
        .post(paymentController.postPayment)

    router.route('/payment/:payment_id')
        .put(paymentController.putPayment);

    router.route('/payment/user/:user_id')
        .get(paymentController.getPayment);

    return router;
}
