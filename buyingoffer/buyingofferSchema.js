// Load required packages
var mongoose = require('mongoose');
var Category = require('../category/categorySchema')

// Define our movie schema
var BuyingofferSchema   = new mongoose.Schema({
    title: String,
    description: String,
    condition: String,
    price: Number,
    validationdate: Date,
    requirepictures: {type: Boolean, default: false},
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    category: Category.categorySchema,
    shippingAddress: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Address'
    },
    isSold: {type: Boolean, default: false},

});

// Export the Mongoose model
module.exports = mongoose.model('Buyingoffer', BuyingofferSchema);
