module.exports = userRoutes;

function userRoutes(passport) {

    var userController = require('./userController');
    var router = require('express').Router();


    router.post('/login', userController.login);
    router.post('/signup', userController.signup);
    router.post('/unregister', passport.authenticate('jwt', {session: false}),userController.unregister);

    router.get('/users/profile', passport.authenticate('jwt', {session: false}), userController.profile);
    router.route('/users/getName/:user_id')
        .get(userController.getName);
    
    router.route('/users/getRating/:user_id')
        .get(userController.getRating);
    
    return router;
}
