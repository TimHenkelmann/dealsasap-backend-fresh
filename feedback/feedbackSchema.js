/**
 * Created by yunusemre on 24.06.2016.
 */

var mongoose = require('mongoose');

var FeedbackSchema   = new mongoose.Schema({
    transaction: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Offerresponse'
    },
    title: {
        type: String,
        default: ''
    },
    description: {
        type: String,
        default: ''
    },
    date: Date,
    rating: {
        type: Number,
        default: 0
    }
});

// Export the Mongoose model
module.exports = mongoose.model('Feedback', FeedbackSchema);
