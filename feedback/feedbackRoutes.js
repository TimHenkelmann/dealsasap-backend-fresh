/**
 * Created by yunusemre on 24.06.2016.
 */

module.exports = feedbackRoutes;

function feedbackRoutes(passport) {

    var feedbackController = require('./feedbackController');
    var router = require('express').Router();
    var unless = require('express-unless');

    var mw = passport.authenticate('jwt', {session: false});
    mw.unless = unless;

    //middleware
    router.use(mw.unless({method: ['GET', 'OPTIONS']}));

    router.post('/feedbacks', passport.authenticate('jwt', {session: false}), feedbackController.postFeedback);

    router.route('/feedbacks/:user_id')
        .get(feedbackController.getFeedbacks);

    router.route('/feedback/:feedback_id')
        .get(feedbackController.getFeedback)
        .put(feedbackController.putFeedback)
        .delete(feedbackController.deleteFeedback);

    router.get('/feedback/if_exist/:transaction_id', feedbackController.ifExist);

    return router;
}
