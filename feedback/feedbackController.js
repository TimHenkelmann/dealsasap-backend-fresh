var Feedback = require('./feedbackSchema');
var User = require('../user/userSchema');


exports.postFeedback = function(req, res) {
    var feedback = new Feedback(req.body);

    if (req.user._id != req.body.transaction.addressee) {
        res.sendStatus(401);
        return;
    }

    // add feedback to database
    feedback.save(function(err, m) {
        if (err) {
            res.status(500).send(err);
            return;
        }
        res.status(201).json(m);
    });

    // search for user (seller) and add rating and number of voters to this user
    var ratedUser = req.body.transaction.sender;
    User.findByIdAndUpdate(
        ratedUser,
        { $inc: {       // increase sumRating and numOfVoters
            sumRating: feedback.rating,
            numOfVoters: 1
        } },
        function(err, user) {
            if (err) {
                console.log('user not found');
            }
            else {
                console.log('updated');
            }
        }
    );
};


// Create endpoint /api/feedbacks for GET
exports.getFeedbacks = function(req, res) {
    Feedback.find({'transaction.sender._id' : req.params.user_id}, function(err, feedback) {
        if (err) {
            res.status(500).send(err)
            return;
        };

        res.json(feedback);
    });
};


// Create endpoint /api/feedbacks/:feedback_id for GET
exports.getFeedback = function(req, res) {
    Feedback.findById(req.params.feedback_id, function(err, feedback) {
        if (err) {
            res.status(500).send(err)
            return;
        };

        res.json(feedback);
    });
};

// Create endpoint /api/buyingoffers/:feedback_id for PUT
exports.putFeedback = function(req, res) {
    Feedback.findByIdAndUpdate(
        req.params.feedback_id,
        req.body,
        {
            //pass the new object to cb function
            new: true,
            //run validations
            runValidators: true
        }, function (err, feedback) {
            if (err) {
                res.status(500).send(err);
                return;
            }
            res.json(feedback);
        });

};

// Create endpoint /api/buyingOffers/:buyingoffer_id for DELETE
exports.deleteFeedback = function(req, res) {
    Feedback.findById(req.params.feedback_id, function(err, feedback) {
        if (err) {
            res.status(500).send(err)
            return;
        };

        //authorize
        if (feedback.transaction.sender && req.user.equals(feedback.transaction.sender)) {
            feedback.remove();
            res.sendStatus(200);
        } else {
            res.sendStatus(401);
        }

    });
};

// Check if there exist a feedback from this TA or not
exports.ifExist = function(req, res) {
    Feedback.findOne({ transaction: req.params.transaction_id },
        function(err, feedback) {
            if (err) {
                res.status(500).send(err)
                return;
            }
            var exist = { exist: (feedback != null) };
            res.json(exist);
        }
    );
};
