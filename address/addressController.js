var Address = require('./addressSchema');
var BuyingOffer = require('../buyingoffer/buyingofferSchema');

exports.saveAddress = function(req, res) {
    var address = new Address(req.body);
    if (req.user._id != req.body.user) {
        res.sendStatus(401);
        return;
    }

    address.save(function(err, address) {
        if (err) {
            res.status(500).send(err);
            return;
        }
        res.json(address);
    });
};

exports.getAddresses = function(req, res) {
    var user = req.user._id;

    Address.find({      // get address of specific user
        user: user
    }, function(err, addresses) {
        if (err) {
            res.status(500).send(err);
            return;
        }
        else {
            res.json(addresses);
        }
    });
};

exports.getAddressByBuyingOffer = function(req, res) {
    var buyingoffer_id = req.params.buyingoffer_id;

    // searching buying offer first
    BuyingOffer.findById(buyingoffer_id
    , function(err, buyingoffer) {
        if (err) {
            res.status(500).send(err);
            return;
        }
        // then searching address using address id from buying offer
        Address.findById(buyingoffer.shippingAddress
        , function(err, address) {
            if (err) {
                res.status(500).send(err);
                return;
            }
            res.json(address);
        })
    });
};
