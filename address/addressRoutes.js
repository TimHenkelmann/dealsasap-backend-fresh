module.exports = addressRoutes;

function addressRoutes(passport) {

    var addressController = require('./addressController');
    var router = require('express').Router();
    var unless = require('express-unless');

    var mw = passport.authenticate('jwt', {session: false});
    mw.unless = unless;

    //middleware
    router.use(mw.unless({method: ['GET', 'OPTIONS']}));

    router.post('/address', passport.authenticate('jwt', {session: false}), addressController.saveAddress);
    router.get('/address', passport.authenticate('jwt', {session: false}), addressController.getAddresses);
    router.get('/address/:buyingoffer_id', addressController.getAddressByBuyingOffer);

    return router;
}
