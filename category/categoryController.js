var Category = require('./categorySchema');



exports.postCategory = function(req, res) {

    var category = new Category(req.body);

    category.save(function(err, m) {
        if (err) {
            res.status(500).send(err);
            return;
        }

        res.status(201).json(m);
    });
};


// Create endpoint /api/buyingoffers for GET
exports.getCategories = function(req, res) {
    Category.find(function(err, categories) {
        if (err) {
            res.status(500).send(err);
            return;
        }
        res.json(categories);
    });
};


// Create endpoint /api/buyingoffers/:buyingoffer_id for GET
exports.getCategory = function(req, res) {
    Category.findById(req.params.category_id, function(err, category) {
        if (err) {
            res.status(500).send(err)
            return;
        };

        res.json(category);
    });
};

// Create endpoint /api/buyingoffers/:buyingoffer_id for PUT
exports.putCategory = function(req, res) {
    Category.findByIdAndUpdate(
        req.params.category_id,
        req.body,
        {
            //pass the new object to cb function
            new: true,
            //run validations
            runValidators: true
        }, function (err, category) {
            if (err) {
                res.status(500).send(err);
                return;
            }
            res.json(category);
        });

};

// Create endpoint /api/buyingOffers/:buyingoffer_id for DELETE
exports.deleteCategory = function(req, res) {
    Category.findById(req.params.category_id, function(err, category) {
        if (err) {
            res.status(500).send(err)
            return;
        };

        //authorize
        if (category.user && req.user.equals(category.user)) {
            category.remove();
            res.sendStatus(200);
        } else {
            res.sendStatus(401);
        }

    });
};


exports.getCategoryByParent = function(req, res) {
    Category.find({parent: req.params.category_id}, function(err, category) {
        if (err) {
            res.status(500).send(err)
            return;
        };

        res.json(category);
    });
};
