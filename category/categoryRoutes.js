module.exports = categoryRoutes;


function categoryRoutes(passport) {

    var categoryController = require('./categoryController');
    var router = require('express').Router();
    var unless = require('express-unless');

    var mw = passport.authenticate('jwt', {session: false});
    mw.unless = unless;

    //middleware
    router.use(mw.unless({method: ['GET', 'OPTIONS']}));

    router.route('/categories')
        .post(categoryController.postCategory)
        .get(categoryController.getCategories);

    router.route('/categories/:category_id')
        .get(categoryController.getCategory)
        .put(categoryController.putCategory)
        .delete(categoryController.deleteCategory);
    router.route('/categories/parent/:category_id')
        .get(categoryController.getCategoryByParent);
    return router;
}



