// Load required packages
var mongoose = require('mongoose');


// Define our movie schema
var categorySchema   = new mongoose.Schema({
    _id: {type: String},
    parent: {
        type: String,
        ref: 'CategoryRef'
    },
    ancestors: [{
        type: String,
        ref: 'CategoryRef'
    }]
});

// Export the Mongoose model
module.exports = mongoose.model('CategoryRef', categorySchema);
module.exports.categorySchema = categorySchema;