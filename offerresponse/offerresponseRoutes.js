/**
 * Created by TimH on 22-Jun-16.
 */
module.exports = offerresponseRoutes;

function offerresponseRoutes(passport) {

    var offerresponseController = require('./offerresponseController');
    var router = require('express').Router();
    var unless = require('express-unless');
    var multiparty = require('connect-multiparty');
    var multipartyMiddleware = multiparty();

    var mw = passport.authenticate('jwt', {session: false});
    mw.unless = unless;

    //middleware
    router.use(mw.unless({method: ['GET', 'OPTIONS']}));

    router.route('/offerresponse')
        .post(multipartyMiddleware, offerresponseController.postOfferresponse)
        .get(offerresponseController.getOfferresponses);

    router.route('/offerresponse/:offerresponse_id')
        .get(offerresponseController.getOfferresponse)
        .put(offerresponseController.putOfferresponse);

    router.route('/offerresponse/buyingoffer/:buyingoffer_id')
        .get(offerresponseController.getOfferresponseByBuyingoffer);

     router.route('/offerresponse/user/:user_id')
         .get(offerresponseController.getOfferresponseByUser);

    router.route('/offerresponse/Addressee/:user_id')
        .get(offerresponseController.getOfferresponseByAddressee);

    router.route('/offerresponse/Sender/:user_id')
        .get(offerresponseController.getOfferresponseBySender);

    return router;
}
