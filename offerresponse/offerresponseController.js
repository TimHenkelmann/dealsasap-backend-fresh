/**
 * Created by TimH on 22-Jun-16.
 */
var Offerresponse = require('./offerresponseSchema');


exports.postOfferresponse = function (req, res) {

    var offerresponse = new Offerresponse(req.body);
    var fs = require('fs');

    if (req.files) {
        var fileArray = req.files.file;

        function base64_encode(file) {
            // read binary data
            var bitmap = fs.readFileSync(file);
            // convert binary data to base64 encoded string
            return new Buffer(bitmap).toString('base64');
        }

        offerresponse.pictures = fileArray;
        if (fileArray) {
            //console.log('found files');
            for (var i = 0, len = fileArray.length; i < len; i++) {
                offerresponse.pictures[i].data = base64_encode(req.files.file[i].path);
                offerresponse.pictures[i].contentType = req.files.file[i].type;
                offerresponse.pictures[i].name = req.files.file[i].name;
            }
        }
    }
    offerresponse.save(function (err, m) {
        if (err) {
            res.send(err);
            return;
        }
        res.status(201).json(m);
    });
};

exports.putOfferresponse = function (req, res) {
    Offerresponse.findByIdAndUpdate(
        req.params.offerresponse_id,
        req.body,
        {
            //pass the new object to cb function
            new: true,
            //run validations
            runValidators: true
        }, function (err, offerresponse) {
            if (err) {
                res.status(500).send(err);
                return;
            }
            res.json(offerresponse);
        });

};

exports.getOfferresponses = function (req, res) {
    Offerresponse.find(function (err, offerresponses) {
        if (err) {
            res.status(500).send(err);
            return;
        }
        res.json(offerresponses);
    });
};

// Create endpoint /api/buyingoffers/:buyingoffer_id for GET
exports.getOfferresponse = function (req, res) {
    Offerresponse.findById(req.params.offerresponse_id, function (err, offerresponse) {

        if (err) {
            res.status(500).send(err)
            return;
        }

        res.json(offerresponse);
    });
};

exports.getOfferresponseByAddressee = function (req, res) {
    Offerresponse.find({'addressee': req.params.user_id}, function (err, offerresponses) {

        if (err) {
            res.status(500).send(err)
            return;
        }
        ;

        res.json(offerresponses);
    });
};

exports.getOfferresponseBySender = function (req, res) {
    Offerresponse.find({'sender': req.params.user_id}, function (err, offerresponses) {

        if (err) {
            res.status(500).send(err)
            return;
        }

        res.json(offerresponses);
    });
};

exports.getOfferresponseByUser = function (req, res) {
    Offerresponse.find({
        $or: [{'sender': req.params.user_id},
              {'addressee': req.params.user_id}]}
        , function (err, offerresponses) {

        if (err) {
            res.status(500).send(err)
            return;
        }

        res.json(offerresponses)
    });
};

exports.getOfferresponseByBuyingoffer = function(req, res) {
    Offerresponse.find({'buyingoffer': req.params.buyingoffer_id}, function (err, offerresponses) {

        if (err) {
            res.status(500).send(err)
            return;
        }
        res.json(offerresponses);
    });
};
