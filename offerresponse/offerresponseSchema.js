/**
 * Created by TimH on 22-Jun-16.
 */

var mongoose = require('mongoose');


// Define our movie schema
var OfferresponseSchema   = new mongoose.Schema({
    buyingoffer: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Buyingoffer'
    },
    addressee:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Address'
    },
    sender:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    pictures: [{
        data: String, contentType: String, name: String
        }],
    message: String,
    date: Date,
    isTransaction: {type: Boolean, default: false},
    isResponded: {type: Boolean, default: false}
});

// Export the Mongoose model
module.exports = mongoose.model('Offerresponse', OfferresponseSchema);
